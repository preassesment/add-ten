package com.tmobile.bootcamp.model;


import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@Validated
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AddTenResponse {
	
	@JsonProperty("sum")
	private int sum;

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	@Override
	public String toString() {
		return "AddTenResponse [sum=" + sum + "]";
	}
	
	

}
