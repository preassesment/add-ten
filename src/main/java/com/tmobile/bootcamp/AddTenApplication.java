package com.tmobile.bootcamp;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan(basePackages = "com.tmobile.bootcamp.controller")
@EntityScan("com.tmobile.bootcamp.controller")
public class AddTenApplication implements CommandLineRunner {

	private static final Logger log = LogManager.getLogger(AddTenApplication.class);

	

	public static void main(String[] args) {
		SpringApplication.run(AddTenApplication.class, args);

	}

	public void run(String... args) throws Exception {
		log.info("Apllication ready to use...");
	}

	

}
