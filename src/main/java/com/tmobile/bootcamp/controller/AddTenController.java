package com.tmobile.bootcamp.controller;


import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tmobile.bootcamp.model.AddTenResponse;

@RestController
@RequestMapping("/")
public class AddTenController {

	private static final Logger logger = LogManager.getLogger(AddTenController.class);

	@GetMapping(value = "/{number}", produces = { "application/json" })
	public ResponseEntity<AddTenResponse> addTen(@Valid @PathVariable("number") String number) throws Exception {
		try {
			logger.debug(number);
			int intNumber = Integer.parseInt(number);
			AddTenResponse addTenRespone = new AddTenResponse();
			addTenRespone.setSum(intNumber + 10);
			return new ResponseEntity<AddTenResponse>(addTenRespone, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error adding Ten ", e);	
			throw e;
		}

	}

}
